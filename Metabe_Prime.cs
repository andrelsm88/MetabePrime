﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Robocode;
using System.Drawing;


namespace ALS
{
    class Metabe_Prime
    {
        class MetabePrime : Robot
        {
            
          public override void Run()
          {

                Color green = Color.FromName("Green"); 
                Color black = Color.FromName("Black"); 
                Color white = Color.FromName("White"); 
                SetColors(green, black, white);

                Ahead(500);

            while (true)
            {
                    TurnRight(180);
                    TurnGunLeft(90);

                    for (int voltasDir = 0; voltasDir < 4; voltasDir++)
                    {
                        Ahead(300);
                        TurnRight(90);
                        TurnGunRight(90);
                    }

                    for (int voltasEsq = 4; voltasEsq > 0; voltasEsq--)
                    {
                        Back(200);
                        TurnLeft(90);
                        TurnGunLeft(180);
                    }
            }


          }
            public override void OnScannedRobot(ScannedRobotEvent e)
            {
                
                Fire(2);
            }

            public override void OnHitByBullet(HitByBulletEvent e)
            {
               Back(60);
            }

            public override void OnHitRobot(HitRobotEvent evnt)
            {
                if (evnt.IsMyFault)
                {
                    Back(400);



                }
            }
        }
    }
}

